#from enum import IntEnum

input = "R4, R5, L5, L5, L3, R2, R1, R1, L5, R5, R2, L1, L3, L4, R3, L1, L1, R2, R3, R3, R1, L3, L5, R3, R1, L1, R1, R2, L1, L4, L5, R4, R2, L192, R5, L2, R53, R1, L5, R73, R5, L5, R186, L3, L2, R1, R3, L3, L3, R1, L4, L2, R3, L5, R4, R3, R1, L1, R5, R2, R1, R1, R1, R3, R2, L1, R5, R1, L5, R2, L2, L4, R3, L1, R4, L5, R4, R3, L5, L3, R4, R2, L5, L5, R2, R3, R5, R4, R2, R1, L1, L5, L2, L3, L4, L5, L4, L5, L1, R3, R4, R5, R3, L5, L4, L3, L1, L4, R2, R5, R5, R4, L2, L4, R3, R1, L2, R5, L5, R1, R1, L1, L5, L5, L2, L1, R5, R2, L4, L1, R4, R3, L3, R1, R5, L1, L4, R2, L3, R5, R3, R1, L3"

#class Direction(IntEnum):
#    up, right, down, left = range(4)

direction = 0                   # up
position = [0, 0]               # [x, y]
positions = []
locationVisitTwice = None

def cycleDirection(direction, moveRight):
    if moveRight:
        if direction + 1 > 3:
            direction = 0
        else:
            direction += 1
    else:
        if direction - 1 < 0:
            direction = 3
        else:
            direction -= 1

    return direction

def checkPosition(position):
    global locationVisitTwice

    if locationVisitTwice == None:
        for p in positions:
            if p == position:
                locationVisitTwice = position[:]
                break

for i in input.split(", "):
    direction = cycleDirection(direction, i.startswith("R"))
    blocks = int(i[1:])

    if direction == 0:
        for j in range(1, blocks + 1):
            p = position[:]
            p[1] += j
            checkPosition(p)
            positions.append(p)
        position[1] += blocks
    elif direction == 1:
        for j in range(1, blocks + 1):
            p = position[:]
            p[0] += j
            checkPosition(p)
            positions.append(p)
        position[0] += blocks
    elif direction == 2:
        for j in range(1, blocks + 1):
            p = position[:]
            p[1] -= j
            checkPosition(p)
            positions.append(p)
        position[1] -= blocks
    elif direction == 3:
        for j in range(1, blocks + 1):
            p = position[:]
            p[0] -= j
            checkPosition(p)
            positions.append(p)
        position[0] -= blocks

length1 = abs(position[0]) + abs(position[1])
length2 = abs(locationVisitTwice[0]) + abs(locationVisitTwice[1])

print("Part 1: Easter Bunny HQ is {} blocks away!".format(length1))
print("Part 2: Easter Bunny HQ is {} blocks away!".format(length2))